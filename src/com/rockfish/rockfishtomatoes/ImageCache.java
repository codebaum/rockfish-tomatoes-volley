package com.rockfish.rockfishtomatoes;

import java.util.HashMap;

import android.graphics.drawable.Drawable;
import android.util.Log;

public class ImageCache {

    private static final String TAG = ImageCache.class.getSimpleName();
    private static ImageCache _instance;
    private static HashMap<String, Drawable> cachedImages;

    private ImageCache() {
        Log.v(TAG, "ImageCache()");
        cachedImages = new HashMap<String, Drawable>();
    }

    public static ImageCache getInstance() {
        Log.v(TAG, "getInstance()");
        if (_instance == null) {
            _instance = new ImageCache();
        }
        return _instance;
    }

    public void addImageToCache(String url, Drawable drawable) {
        Log.v(TAG, "addImageToCache()");
        if (!cachedImages.containsKey(url)) {
            cachedImages.put(url, drawable);
        }
    }

    public Drawable getImageFromCache(String url) {
        Log.v(TAG, "getImageFromCache()");
        if (cachedImages != null) {
            return cachedImages.get(url);
        }
        return null;
    }

}
