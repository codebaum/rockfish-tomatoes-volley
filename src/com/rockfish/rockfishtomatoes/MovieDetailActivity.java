package com.rockfish.rockfishtomatoes;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.rockfish.rockfishtomatoes.model.Movie;

public class MovieDetailActivity extends FragmentActivity {

    public static final String EXTRA_URL = "url";
    private static final String TAG = MovieDetailActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.v(TAG, "onCreate()");

        // Need to check if Activity has been switched to landscape mode
        // If yes, finished and go back to the start Activity
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            finish();
            return;
        }
        setContentView(R.layout.activity_movie_detail);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            Movie movie = (Movie) extras.getParcelable(EXTRA_URL);
            ImageView thumb = (ImageView) findViewById(R.id.movieThumb);
            TextView view = (TextView) findViewById(R.id.movieTitle);
            view.setText(movie.getMovieTitle());
            thumb.setImageDrawable(ImageCache.getInstance().getImageFromCache(movie.getPosters().thumbnail));
        }
    }

}
